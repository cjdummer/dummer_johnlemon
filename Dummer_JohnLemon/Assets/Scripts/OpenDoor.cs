﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoor : MonoBehaviour
{
    //This script handles opening a door using the box on a pedestal trigger. 
    public GameObject Door;
    Rigidbody DoorRb;
    bool isOpen;
    // Start is called before the first frame update
    void Start()
    {
        DoorRb = Door.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay(Collider other)
    {
        if (((other.CompareTag("Pickup")) || (other.CompareTag("Statue"))) && !isOpen) //opens door if it is not already open and a box or statue is on the trigger.
        {
            DoorRb.transform.Rotate(0, -90, 0);
            isOpen = true;
        }
        return;
    }

    private void OnTriggerExit(Collider other) //closes door if box taken off of trigger
    {
        if(other.CompareTag("Pickup") && isOpen)
        {
            DoorRb.transform.Rotate(0, 90, 0);
            isOpen = false;
        }
    }
}
