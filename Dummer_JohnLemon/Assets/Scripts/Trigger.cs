﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger : MonoBehaviour
{ //Simple script to attach to a trigger, this is referenced by the two triggers script to check whether the switches have a statue on them.
    public bool isTriggered;

    
    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Statue"))
        {
            isTriggered = true;
        }
    }

}
