﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TwoTriggers : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject switch1;
    public GameObject switch2;
    Trigger switchScript1;
    Trigger switchScript2;
    bool open = false;
    Rigidbody rb;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        switchScript1 = switch1.GetComponent<Trigger>(); //references the Trigger script to see if switches have statues on them.
        switchScript2 = switch2.GetComponent<Trigger>();
    }

    // Update is called once per frame
    void Update()
    {
        if (switchScript1.isTriggered && switchScript2.isTriggered && !open)
        {
            rb.transform.Rotate(0, -90, 0);
            open = true;
        }
    }
}
