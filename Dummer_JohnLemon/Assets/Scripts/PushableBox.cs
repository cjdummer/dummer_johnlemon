﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushableBox : MonoBehaviour
{
    //This script handles being able to push objects from one specific direction, used on statues in this particular game.

        public enum Sides { Left, Up, Right, Down};


    public Sides sideToPush;
    GameObject player;
    Rigidbody rb;
    
    GameObject pickup; //references the pickup script to see if player is holding a pickup.
    Pickup otherScript;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        player = GameObject.FindGameObjectWithTag("Player");
        pickup = GameObject.FindGameObjectWithTag("Pickup");
        otherScript = pickup.GetComponent<Pickup>();
    }

    // Update is called once per frame
    void Update()
    {
        if (player != null && otherScript.isHolding == false) //if player exists and is not holding an object, they can push
        {
            rb.isKinematic = true; //while kinematic, cant be moved

            if(sideToPush == Sides.Left && player.transform.position.x < transform.position.x) //if player is to the left of object, can be moved
            {
                rb.isKinematic = false;
                
            }
            else if (sideToPush == Sides.Up && player.transform.position.z > transform.position.z) //if player is above object, can be moved
            {
                rb.isKinematic = false;
                
            }
            else if (sideToPush == Sides.Right && player.transform.position.x > transform.position.x) //if player is right of object, can be moved
            {
                rb.isKinematic = false;
                
            }
            else if (sideToPush == Sides.Down && player.transform.position.z < transform.position.z) //if player is below object, can be moved.
            {
                rb.isKinematic = false;
                
            }
        }

      
    }

    private void OnTriggerEnter(Collider other) //checks if what is pushing is the player.
    {
        if (other.tag != "Player"){
            
        }
      
    }

  

 
}
