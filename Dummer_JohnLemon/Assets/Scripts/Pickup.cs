﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup: MonoBehaviour
{   //This script handles picking up and dropping boxes.
    int timer;
    Rigidbody item;
    public GameObject player;
    public bool isHolding = false;

    // Start is called before the first frame update
    void Start()
    {
        item = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
       HandleCratePosition(); //manages position of the pickup depending on whether you've pressed F or not
    }

   

    private void HandleCratePosition() //handles how the crate should react when picked up and dropped
    {
        if (isHolding && Input.GetKeyDown(KeyCode.F) && timer > 10) //after 10 frames, can press f again to drop box
        {
            isHolding = false;
           // objectPosition = item.transform.position + new Vector3(0, 0, -10);
            item.transform.SetParent(null);
            item.useGravity = true;
            item.transform.position = item.transform.position;
            timer = 0;
        }
        else if (isHolding) //while held, object stays in front of player
        {
            timer++;
            item.velocity = Vector3.zero;
            item.angularVelocity = Vector3.zero;
            item.transform.position = player.transform.position + new Vector3(.75f,1,0f);
        }

        



    }

    private void OnTriggerStay(Collider other) //while the player is in the trigger and presses f, they pick up the crate
    {
        if (other.CompareTag("Player") && Input.GetKeyDown(KeyCode.F))
        {
            isHolding = true;
            item.useGravity = false;
            item.detectCollisions = true;
            
           
        }
       
       

       
    }


}
